﻿#include "sea.h"
#include "ui_sea.h"

Sea::Sea(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::Sea)
{
    ui->setupUi(this);
    ui->tableWidget->setColumnCount(10);
    ui->tableWidget->setRowCount(10);
}

Sea::~Sea()
{
    delete ui;
}

void Sea::changeEvent(QEvent *e)
{
    QFrame::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Sea::resizeEvent(QResizeEvent *){
for(int i=0;i<10;++i){
        ui->tableWidget->setColumnWidth(i,(ui->tableWidget->width()-20)/10);
        ui->tableWidget->setRowHeight(i,(ui->tableWidget->height()-20)/10);    }
}
