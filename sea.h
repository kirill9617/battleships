﻿#ifndef SEA_H
#define SEA_H

#include <QFrame>

namespace Ui {
class Sea;
}

class Sea : public QFrame
{
    Q_OBJECT

public:
    explicit Sea(QWidget *parent = 0);
    ~Sea();

    void resizeEvent(QResizeEvent *);
protected:
    void changeEvent(QEvent *e);

private:
    Ui::Sea *ui;
};

#endif // SEA_H
