#-------------------------------------------------
#
# Project created by QtCreator 2014-06-13T13:41:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Battleships
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    gamemanager.cpp \
    sea.cpp \
    ship.cpp \
    player.cpp \
    ai.cpp \
    human.cpp \
    cell.cpp

HEADERS  += mainwindow.h \
    gamemanager.h \
    sea.h \
    ship.h \
    player.h \
    ai.h \
    human.h \
    cell.h

FORMS    += mainwindow.ui \
    sea.ui

CONFIG += mobility
MOBILITY = 

